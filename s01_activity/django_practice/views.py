from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse

from .models import GroceryItem

from django.template import loader

def index(request):
	groceryitem_list = GroceryItem.objects.all()
	template = loader.get_template("index.html")
	context = {
		'groceryitem_list': groceryitem_list
	}
	return HttpResponse(template.render(context, request))

def groceryitem(request,groceryitem_id):
	response = f"you are viewing the details of {groceryitem_id}"
	return HttpResponse(response)